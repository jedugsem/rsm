use orbtk::prelude::*;
use std::process;
fn main() {
    Application::new()
        .window(|ctx| {
            Window::new()
                .title("OrbTk - minimal example")
                .child(NavigatuonView::new().build(ctx))
                .resizeable(true)
                .build(ctx)
        })
        .run();
}

static ID_NAVIGATION_MASTER_DETAIL: &str = "id_navigation_master_detail";
static HEADER_HEIGHT: i32 = 40;
#[derive(Debug, Default, AsAny)]
struct NavigationState {
    master_detail: Entity,
    active: bool,
}

impl State for NavigationState {
    fn init(&mut self, _registry: &mut Registry, ctx: &mut Context) {
        self.master_detail = ctx.child(ID_NAVIGATION_MASTER_DETAIL).entity();
    }
    fn messages(
        &mut self,
        mut messages: MessageReader,
        _registry: &mut Registry,
        ctx: &mut Context,
    ) {
        for message in messages.read::<MasterDetailAction>() {
            ctx.send_message(message, self.master_detail);
        }
    }
}

widget!(
    NavigatuonView<NavigationState> {
        md_navigation_visibility : Visibility
    }
);
impl Template for NavigatuonView {
    fn template(self, id: Entity, ctx: &mut BuildContext) -> Self {
        let pager = Pager::new()
            .child(Button::new().text("ss").build(ctx))
            .child(Container::new().build(ctx))
            .build(ctx);
        let pager_head = Pager::new()
            .child(
                TextBlock::new()
                    .text("1.page")
                    .foreground("black")
                    .build(ctx),
            )
            .child(
                TextBlock::new()
                    .text("2.page")
                    .foreground("black")
                    .build(ctx),
            )
            .h_align("center")
            .v_align("center")
            .build(ctx);

        self.child(
            MasterDetail::new()
                .id(ID_NAVIGATION_MASTER_DETAIL)
                .responsive(true)
                .break_point(800)
                .navigation_visibility(("md_navigation_visibility", id))
                .on_changed("", |ctx, _| {
                    println!("hey");
                })
                .master_detail(
                    Stack::new()
                        .child(
                            Container::new()
                                .padding(4)
                                .height(HEADER_HEIGHT)
                                .background("lightgrey")
                                .child(
                                    Button::new()
                                        .text("X")
                                        .style("button_single_content")
                                        .visibility(("md_navigation_visibility", id))
                                        .h_align("end")
                                        .on_click(move |ctx, _| {
                                            process::exit(1);
                                            true
                                        })
                                        .build(ctx),
                                )
                                .child(
                                    TextBlock::new()
                                        .text("RSA")
                                        .foreground("black")
                                        .h_align("center")
                                        .v_align("center")
                                        .build(ctx),
                                )
                                .build(ctx),
                        )
                        .child(
                            Button::new()
                                .style("button_single_content")
                                .text("1. pane")
                                .on_click(move |ctx, _| {
                                    ctx.send_message(MasterDetailAction::ShowDetail, id);
                                    ctx.send_message(PagerAction::Navigate(0), pager);
                                    ctx.send_message(PagerAction::Navigate(0), pager_head);
                                    true
                                })
                                .build(ctx),
                        )
                        .child(
                            Button::new()
                                .style("button_single_content")
                                .text("2. pane")
                                .on_click(move |ctx, _| {
                                    ctx.send_message(MasterDetailAction::ShowDetail, id);
                                    ctx.send_message(PagerAction::Navigate(1), pager);
                                    ctx.send_message(PagerAction::Navigate(1), pager_head);
                                    true
                                })
                                .build(ctx),
                        )
                        .build(ctx),
                    Stack::new()
                        .child(
                            Container::new()
                                .padding(0)
                                .height(HEADER_HEIGHT)
                                .background("lightgrey")
                                .child(
                                    Container::new()
                                        .width(1)
                                        .height(HEADER_HEIGHT)
                                        .background("black")
                                        .build(ctx),
                                )
                                .child(
                                    Container::new()
                                        .padding(4)
                                        .height(HEADER_HEIGHT)
                                        .child(
                                            Button::new()
                                                .icon(material_icons_font::MD_KEYBOARD_ARROW_LEFT)
                                                .style("button_single_content")
                                                .visibility(("md_navigation_visibility", id))
                                                .border_width(15)
                                                .border_radius(4)
                                                .h_align("start")
                                                .on_click(move |ctx, _| {
                                                    ctx.send_message(
                                                        MasterDetailAction::ShowMaster,
                                                        id,
                                                    );
                                                    true
                                                })
                                                .build(ctx),
                                        )
                                        .child(
                                            Button::new()
                                                .text("X")
                                                .style("button_single_content")
                                                .h_align("end")
                                                .on_click(move |ctx, _| {
                                                    process::exit(1);
                                                    true
                                                })
                                                .build(ctx),
                                        )
                                        .build(ctx),
                                )
                                .child(pager_head)
                                .build(ctx),
                        )
                        .child(
                            Container::new()
                                .v_align("strech")
                                .child(
                                    Container::new()
                                        .background("black")
                                        .width(1)
                                        .height(1080)
                                        .v_align("strech")
                                        .build(ctx),
                                )
                                .child(Container::new().padding(1).child(pager).build(ctx))
                                .build(ctx),
                        )
                        .build(ctx),
                )
                .build(ctx),
        )
    }
}
